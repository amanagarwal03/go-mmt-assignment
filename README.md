# Go-MMT Assignment

#STEPS TO RUN THE PROJECT
1. Clone the project:  https://bitbucket.org/amanagarwal03/go-mmt-assignment/src/master/
2. Build the docker image from the files using command: docker build . --build-arg CORPUS_FILE=corpus.csv --build-arg PROPERTY_FILE=application.properties --build-arg JAR_FILE=go-mmt-1.0.jar -t gommt
3. Run the docker image using command: docker run -d -p 8080:8080 gommt
You can change the path of csv file in application.properties.
4. Use the following postman collection to test the API: https://www.getpostman.com/collections/0e15071093011e2df4c2


#DETAILS ABOUT THE PROJECT

#This Project runs on following port:
Port:8080

#API call
API - http://localhost:8080/one

#API response
{
  "key": "one",
  "value": 1
}

#The project implements the following things:

1. Loads a CSV file(corpus.csv) on startup.
2. Exposes an API to explore the CSV file.

#The web application handles the following cases:

1. If the id passed is not in CSV file, then the API returns that data doesn't exists in CSV.
2. The API call is not done properly, then the API asks to enter a valid key.

#Assumptions:

1. Given is a csv file
2. Columns name are key and value


#Execution Instructions:

1. change file path in application.properties
2. Run ./gradlew clean build
3. You will find a jar(go-mmt-1.0.jar) in build/libs


#I have attached Screenshots of the web appliaction with all the cases mentioned. 


#DOCKERIZATION

#Command to build docker image from your files. Run this from directory where files like property files and jar are present.
docker build .  --build-arg CORPUS_FILE=corpus.csv --build-arg PROPERTY_FILE=application.properties --build-arg JAR_FILE=go-mmt-1.0.jar -t gommt

# Command to run docker container from image "gommt" in attached mode
docker run -p 8080:8080 gommt

# In detached there will not be any console prints. you will be free in terminal.
docker run -d -p 8080:8080 gommt

# How to test:
Test from the given Postman collection link.
Or you can hit the following URL from your browser: http://localhost:8080/one


#POSTMAN COLLECTION LINK

https://www.getpostman.com/collections/0e15071093011e2df4c2
This link contains API for this web application
