package com.service;

import com.fasterxml.jackson.databind.MappingIterator;
import com.fasterxml.jackson.dataformat.csv.CsvMapper;
import com.fasterxml.jackson.dataformat.csv.CsvSchema;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;

import javax.annotation.PostConstruct;
import java.io.File;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@org.springframework.stereotype.Service
@Slf4j
public class FileService {
    @Value("${file.path}")
    private String filePath;

    public FileService(){}
    Map<String, Integer> map = new HashMap<>();
    @PostConstruct
    public void init() {
        File file = new File (filePath);
        List<Fields> fields  = readCSVFile(Fields.class,file);
        for (Fields str : fields) {
            map.put(str.getKey(),str.getValue()); }
        if(!fields.isEmpty())
        log.info("CSV has been read successfully from path: "+filePath);
    }


    public <T> List<T> readCSVFile(Class<T> type, File file) {
        try {
            CsvSchema bootstrapSchema = CsvSchema.emptySchema().withHeader();
            CsvMapper mapper = new CsvMapper();
            MappingIterator<T> readValues =
                    mapper.reader(type).with(bootstrapSchema).readValues(file);
            return readValues.readAll();
        } catch (Exception e) {
            log.error("Error occurred while loading object list from file " +  e);
            return Collections.emptyList();
        }
    }
    public int getValue(String key){
        return map.get(key);
    }
}
