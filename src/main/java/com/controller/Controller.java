package com.controller;

import com.service.FileService;
import io.swagger.annotations.Api;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.LinkedHashMap;

@Api(value = "csv explorer", description = "Exposes api to read csv")
@RestController
public class Controller {

    @Autowired
    private FileService fileService;
    LinkedHashMap<String,Object> linkedHashMap = new LinkedHashMap<>();

    @RequestMapping()
    public String defaultCase(){
        String defaultString = "Please provide a VALID API request";
        return defaultString;
    }

    @RequestMapping("/{id}")
    public LinkedHashMap<String,Object> getValue(@PathVariable String id){

        linkedHashMap.put("key",id);
        try {
            int responseValue = fileService.getValue(id);
            linkedHashMap.put("value",responseValue);
        }catch (Exception e){
            linkedHashMap.put("value","Does not exist in CSV");
        }
        return  linkedHashMap;
    }
}
