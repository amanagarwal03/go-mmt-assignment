FROM openjdk:8-jdk-alpine
VOLUME /tmp
ARG JAR_FILE
ARG PROPERTY_FILE
ARG CORPUS_FILE
COPY ${JAR_FILE} app.jar
COPY ${PROPERTY_FILE} application.properties
COPY ${CORPUS_FILE} corpus.csv
ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","app.jar","--spring.config.location=application.properties"]
